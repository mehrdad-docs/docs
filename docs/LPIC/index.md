# LPIC Course Syllabus and Objectives

I've covered LPIC topics here.

This document contains of [LPIC 1 - Exam 101][LPIC101], [LPIC 1 - Exam 102][LPIC102], [LPIC 2][LPIC2] and  [LPIC 7 (DevOps) - Exam 701][LPIC701]

<!-- links -->
[LPIC101]: objectives/lpic1-exam101.md
[LPIC102]: objectives/lpic1-exam102.md
[LPIC2]: objectives/lpic2.md
[LPIC701]: objectives/lpic7-exam701.md
