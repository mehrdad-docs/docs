# Stack.Push(Memory Managements\)

### [Linker & Loader][ll]

### [MMU]

## ELF

- [The ELF Format][ELF-Format]

- [ELF structure][ELF-Structure]

<!-- links -->
[ll]: linker-loader.md
[MMU]: mmu.md
[ELF-Format]: https://www.ics.uci.edu/~aburtsev/238P/hw/hw3-elf/hw3-elf.html
[ELF-Structure]: /docs/assets/mm/elf-1.jpg
