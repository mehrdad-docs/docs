# Linux Security

## Encryption/Decryption

### SSH

### GPG

#### encrypt a file with gpg

```bash
gpg --output file-name.gpg --symmetric file-name
```

#### decrypt encrypted gpg file

```bash
gpg --output file-name --decrypt file-name.gpg
```
