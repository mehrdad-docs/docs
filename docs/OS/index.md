# Operation Systems

This section contain of most three important operation systems.

I've covered [GNU/Linux][GNULinux] in both Server and Desktop side.

I've considered to work with [UNIX] later.

I've covered [Windows] in Desktop side.

[GNULinux]: gnu_linux/index.md
[UNIX]: unix/index.md
[Windows]: windows/index.md
